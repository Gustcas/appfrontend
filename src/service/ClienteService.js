import MasterService from "./MasterService";
import { conf } from "../Config.js";
const CLIENTE_API_URL = conf.url.API_URL + "/api/v1/cliente";

class ClienteService {
    listAllCliente() {
        var CURRENT_API_URL = `${CLIENTE_API_URL}/listAllCliente`;
        console.log(CURRENT_API_URL);
        return MasterService.getDataService(CURRENT_API_URL);
    }

    findClienteByCode(payload) {
        var CURRENT_API_URL = `${CLIENTE_API_URL}/findClienteByCode`;
        console.log(CURRENT_API_URL);
        return MasterService.postDataServiceJSONHeader(CURRENT_API_URL, payload);
    }

    saveCliente(payload) {
        var CURRENT_API_URL = `${CLIENTE_API_URL}/saveCliente`;
        console.log(CURRENT_API_URL);
        return MasterService.postDataServiceJSONHeader(CURRENT_API_URL, payload);
    }

    updateCliente(payload) {
        console.log(payload);
        var CURRENT_API_URL = `${CLIENTE_API_URL}/saveCliente`;
        console.log(CURRENT_API_URL);
        return MasterService.postDataServiceJSONHeader(CURRENT_API_URL, payload);
    }

    deleteById(payload) {
        console.log(payload);
        var CURRENT_API_URL = `${CLIENTE_API_URL}/deleteById/`;
        console.log(CURRENT_API_URL);
        return MasterService.deleteEmptyDataServiceJSONHeader(CURRENT_API_URL, payload);
    }
}
export default new ClienteService();
