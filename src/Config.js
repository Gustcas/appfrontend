const prod = {
    url: {
        API_URL: "http://localhost:8088", /// production
    },
    defaultTimeout: 5 * 3600 * 1000, // Five hours
};
const dev = {
    url: {
        API_URL: "http://localhost:8088", //dev
    },
    defaultTimeout: 2 * 3600 * 1000, // Two hours
};
export const conf = process.env.NODE_ENV === "development" ? dev : prod;
